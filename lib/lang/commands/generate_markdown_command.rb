module Lang
  module GenerateMarkdownCommand
    extend self

    module StringLength
      def string_length(string)
        string.chars.map do |char|
          char.bytesize > 2 ? 2 : 1
        end.sum
      end
    end

    class Word
      include StringLength

      attr_reader :native, :english

      def initialize(native, english)
        @native = native || ''
        @english = english || ''
      end

      def native_length
        @native_length = string_length(native)
      end
    end

    module GenerateSectionBaseCommand
      private

      def render_heading_for(headings, padding, out)
        out.puts heading_for(headings, padding)
      end

      def mask_for(count, sep = nil)
        sep = sep ? " #{sep} " : '   '
        mask_list = ['| %s |'] * count
        base_mask = mask_list.join(sep)
      end

      def heading_for(headings, padding)
        mask = mask_for(headings.count, '%s')

        heading = add_headings(mask, headings, padding)
        heading += "\n"

        lines = ['-' * padding] * headings.count
        heading += add_headings(mask, lines, padding, '-')
      end

      def add_headings(mask, list, padding, sep = ' ')
        args = list.map do |el|
          case el
          when String
            [el, sep]
          else
            pad = ' ' * (padding - el.length - 4)
            display = '**' + el.text + '**' + pad
            [display, sep]
          end
        end.flatten

        mask % args
      end
    end

    module GenerateVerbSectionCommand
      extend GenerateSectionBaseCommand
      extend self

      def execute(data, metadata, out = $stdout)
        render_heading_for metadata.headings, metadata.padding, out
        mask = mask_for(metadata.headings.count)
        data.each do |verb|
          args = metadata.headings.map.each_with_index do |heading, i|
            word = Word.new(verb[heading.key], verb['english'])
            WordFormatterCommand.execute word, metadata, i > 0
          end

          out.puts mask % args
        end
      end

      private
    end

    module WordFormatterCommand
      extend self

      def execute(word, metadata, override_meta = nil)
        padding = (metadata.padding - word.native_length)
        return word.native + ''.ljust(padding) unless metadata.include_translation?
        return word.native + ''.ljust(padding) if override_meta

        english =
          if word.english.respond_to?(:join)
            word.english.join(', ')
          else
            word.english
          end

        word.native + (' : ' + english).ljust(padding)
      end
    end

    module GenerateGenericSectionCommand
      extend GenerateSectionBaseCommand
      extend self

      def execute(data, metadata, out = $stdout)
        headings = [metadata.heading] * metadata.columns
        render_heading_for headings, metadata.padding, out

        data.each_slice(metadata.columns) do |words|
          words.each_with_index do |word, i|
            word = Word.new(word['native'], word['english'])

            out.print '   ' if i > 0
            out.print '| %s |' % [
              WordFormatterCommand.execute(word, metadata)
            ]
          end

          out.puts
        end

        out.puts
      end
    end

    module GenerateSectionCommand
      extend self

      SECTION_COMMANDS = {
        'verbs' => GenerateVerbSectionCommand,
      }

      def execute(data, metadata, section, out = $stdout)
        command = SECTION_COMMANDS[section] || GenerateGenericSectionCommand
        command.execute data, metadata, out
      end
    end

    class Metadata
      attr_reader :lang, :padding

      class Heading
        include StringLength

        attr_reader :text

        def initialize(text)
          @text = text || 'unknown'
        end

        def length
          @length ||= string_length(text)
        end
      end

      def initialize(yaml, lang)
        @lang = lang.strip.downcase
        set_padding yaml['padding']
        on_init(yaml) if respond_to?(:on_init, true)
      end

      def include_translation?
        true
      end

      private

      def set_padding(yaml_padding)
        return @padding = yaml_padding if is_a?(VerbMetadata)
        return @padding = 120 if include_translation?
        @padding = yaml_padding
      end
    end

    class VerbMetadata < Metadata
      attr_reader :headings

      class VerbHeading < Heading
        attr_reader :key

        def initialize(key, text)
          super text
          @key = key
        end
      end

      private

      def on_init(metadata)
        @headings =
          metadata['headings'].map do |key, value|
            VerbHeading.new key, value
          end
      end
    end

    class GenericMetadata < Metadata
      attr_reader :heading, :columns

      private

      def on_init(yaml)
        @heading = Heading.new(yaml['heading'])
        set_columns yaml['columns']
      end

      def set_columns(yaml_columns)
        return @columns = 1 if include_translation?
        @columns = yaml_columns || 1
      end
    end

    def execute
      sources.each do |path|
        yaml = YAML.load_file(path)
        lang = File.basename(path, '.yml')
        yaml['metadata'].keys.each.with_index do |section, i|
          generate_content_for yaml, section, lang, i
        end
      end

      puts ColorMeRad.key('Complete!', :green)
    end

    module GetMetadataCommand
      extend self

      METADATA_CLASS = {
        'verbs' => VerbMetadata,
      }

      def execute(metadata, section, lang)
        klass = METADATA_CLASS[section] || GenericMetadata
        klass.new metadata[section], lang
      end
    end

    private

    def generate_content_for(yaml, section, lang, index)
      data = yaml[section]
      metadata = GetMetadataCommand.execute(yaml['metadata'], section, lang)
      dest = dest_for(metadata, section)

      FileUtils.mkdir_p File.dirname(dest)

      mode = index == 0 ? 'w' : 'a'

      File.open(dest, mode) do |file|
        GenerateSectionCommand.execute data, metadata, section, file
      end

      puts '*' * 80
      system "cat #{dest}"
    end

    def sources
      glob = File.join(Secrets.data_path, '**', '*.yml')
      Dir[glob].sort
    end

    def dest_for(metadata, section)
      #folder = metadata.lang
      file = metadata.lang + '.md'
      #File.join Secrets.docs_path, folder, file
      File.join Secrets.docs_path, file
    end
  end
end

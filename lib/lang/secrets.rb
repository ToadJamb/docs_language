module Lang
  module Secrets
    extend self

    def data_path
      @data_path ||= File.expand_path('data')
    end

    def docs_path
      @docs_path ||= File.expand_path('docs')
    end
  end
end
